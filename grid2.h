#pragma once

#include <vector>
#include <set>
#include <functional>

#include "area.h"

template <class T>
class grid2d {
	float _dx, _dy;
	size_t _fv_count_x;
	size_t _fv_count_y;
	std::vector<std::vector<T>> _grid;

public:
	grid2d(size_t fv_count_x, size_t fv_count_y, float dx, float dy) {
		_fv_count_x = fv_count_x;
		_fv_count_y = fv_count_y;

		_grid.resize(fv_count_x);
		for_each(_grid.begin(), _grid.end(),
			[fv_count_y](vector<T> &e) {e.resize(fv_count_y); });

		_dx = dx;
		_dy = dy;
	}

	std::vector<T> & operator[](size_t i) {
		return _grid[i];
	}
	T & right_edge(size_t i, size_t j) {
		return _grid[i + 1][j];
	}
	T & left_edge(size_t i, size_t j) {
		return _grid[i - 1][j];
	}
	T & top_edge(size_t i, size_t j) {
		return _grid[i][j + 1];
	}
	T & bot_edge(size_t i, size_t j) {
		return _grid[i][j - 1];
	}

	size_t fv_count_x() {
		return _fv_count_x;
	}
	size_t fv_count_y() {
		return _fv_count_y;
	}

	float Sx() {
		return _dy;
	}
	float Sy() {
		return _dx;
	}

	// ���������� ������ ������, ����� ������� �������� ������� y(x)
	std::vector<std::vector<size_t>> get_intersect_y(std::function<float(float)> y) {
		std::set<pair<size_t, size_t>> set;
		std::vector<std::vector<size_t>> res;
		bool is_gap = false;
		size_t i;
		float delta = min(_dx, _dy) / 10;
		for (i = 0; i < _fv_count_x*_dx / delta; ++i) {
			int j = y(i * delta);
			if (j < 0) {
				is_gap = true;
				break;
			}
			j = floor(j / _dy);
			if (j >= 0 && j < _fv_count_y)
				set.insert({ i*delta / _dx, j });
		}
		if (is_gap) {
			i--;
			int j = roundl(y(i * delta) / _dy);
			for (int p = 0; p < j; ++p)
				set.insert({ i*delta / _dx, p });
		}

		for each(auto elem in set) {
			if (res.size() <= elem.first)
				res.push_back(std::vector<size_t>());
			res[elem.first].push_back(elem.second);
		}

		return res;
	}

	area<T> get_upper_area(std::function<float(float)> y) {
		auto intersect = get_intersect_y(y);
		return area<T>(_grid, true, intersect);
	}

	area<T> get_low_area(std::function<float(float)> y) {
		auto intersect = get_intersect_y(y);
		return area<T>(_grid, false, intersect);
	}

	void fill_from_area(area<T> &area) {
		auto boundary = area.get_boundary();
		auto intern = area.get_internal();

		for (size_t i = 0; i < boundary.size(); ++i) {
			for each (size_t j in boundary[i]) {
				_grid[i][j] = area[i][j];
			}
		}
		for (size_t i = 0; i < intern.size(); ++i) {
			for each (size_t j in intern[i]) {
				_grid[i][j] = area[i][j];
			}
		}
	}
};