#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <memory>
#include <functional>
#include <set>
#include <unordered_set>

#include "area.h"
#include "grid2.h"

#define EIGEN_USE_MKL_ALL
#include <Eigen\Dense>

using namespace std;

struct params {
	float Pnet, P, qL, yf, P1 = 0;
};


// ��������� ������� ���������� �� LAPACK_matrix
void fill_from_vector(area<params> &a, Eigen::VectorXf & raw) {
	auto bounds = a.get_boundary();
	auto intrnl = a.get_internal();
	size_t k = 0;
	for (size_t i = 0; i < bounds.size(); ++i)
		for each (size_t j in bounds[i]) {
			size_t pos = a.get_number(i, j);
			a[i][j].P1 = raw(pos);
		}

	for (size_t i = 0; i < intrnl.size(); ++i)
		for each(size_t j in intrnl[i]) {
			size_t pos = a.get_number(i, j);
			a[i][j].P1 = raw(pos);
		}
}

int main() {

	omp_set_num_threads(4);
	Eigen::setNbThreads(4);

	// ������ ������� ����:
	//	1. PartialPivLu - LU ���������� � ��������� ���������.
	//		������������, ��������, ������ ��������
	//  2. colPivHouseholderQr - QR ���������� � ��������� ��������
	//		������������, ����� ���������, ������� ��������. �� �������������
	//		��� ����������� ������� ����� 5000.
	bool isPartialPivLu = true;
	bool isColPivHouseholderQr = false;

	float L0 = 50;				// ��������� ���������, �
	float LMax = 100;			// �������� ������������ ���������, �
	float LMin = 10;			// �������� ����������� ���������, �
	float XMax = LMax;	// ������ ������� ������� X, �
	float YMax = 50;			// ������ ������� ������� Y, �

	size_t Nx = 100;
	size_t Ny = 50;

	float dx = XMax / Nx;
	float dy = YMax / Ny;

	float k = 1e-15;		// �������������, � ^ 2
	float eta = 1e-3;		// ��������, �� �
	float m = 0.2;			// ����������
	float E = 15e+9;		// ������ ����, ��
	float khi = 1;			// ����������������
	float h = 25. / 2;		// �������� ������ �������
	float cmp = 1e-9;		// ����� �����������
	float X0 = k / eta / m / cmp;

	float S0 = 350 * 1.e+5;			// ��������� ����������
	float PW0 = 355 * 1.e+5;        // ��������� �������� �� ��������
	float P0 = 250 * 1.e+5;			// ��������� ��������� ��������

	float WW0 = 5e-3;				// ��������� ������������ ����� ������� �� ��������, �

	double T = 50;
	double dt = 1;

	float w = 0; // h * h / eta / (E * E * E);
	float r = -2 * E * k / 3.14 / h / eta;

	auto current = grid2d<params>(Nx, Ny, dx, dy);
	auto prev = grid2d<params>(Nx, Ny, dx, dy);

	auto y_x = [L0, dy](float x) {return (x <= L0) ? 1.5 * dy : -1; };

	// ua (upper area) - ������� �������. � ��� �������� ��������� �� P1
	auto ua_prev = prev.get_upper_area(y_x);
	auto ua_prev_bounds = ua_prev.get_boundary();
	auto ua_prev_curve_bound = ua_prev.get_curve_boundary();
	auto ua_prev_internal = ua_prev.get_internal();

	// la (lower area) - ������ �������. � ��� �������� ��������� �� Pnet � P
	auto la_prev = prev.get_low_area(y_x);
	auto la_prev_bounds = la_prev.get_boundary();
	auto la_prev_left_bounds = la_prev.get_left_boundary();
	auto la_prev_curve_bound = la_prev.get_curve_boundary();
	auto la_prev_lower_bound = la_prev.get_lower_boundary();
	auto la_prev_internal = la_prev.get_internal();

	/*
	*  ���������� ��������� � ��������� �������
	*/
	// �� ���� �������� ������� ������� ������ P1 = P0
	for (size_t i = 0; i < ua_prev_bounds.size(); ++i)
		for each (size_t j in ua_prev_bounds[i])
			prev[i][j].P1 = P0;

	// �� ������� ������� � �������� ������ P1 = P (�������� ������� �� �� �������)
	for (size_t i = 0; i < ua_prev_curve_bound.size(); ++i)
		for each (size_t j in ua_prev_curve_bound[i])
			prev[i][j].P1 = PW0;

	// �� ������ ������� � �������� ������ P1 = P (�������� ������� �� �� �������)
	size_t i = ua_prev_curve_bound.size() - 1;
	for (size_t j = 0; j < ua_prev_curve_bound[i].size(); ++j)
		prev[i][j].P1 = PW0;

	// ������ ������� ������� ������ P1 = P0
	for (size_t i = 0; i < ua_prev_internal.size(); ++i)
		for each (size_t j in ua_prev_internal[i])
			prev[i][j].P1 = P0;

	// �� �������� ������� ������ ��������� �������
	for (size_t i = 0; i < la_prev_bounds.size(); ++i)
		for each (size_t j in la_prev_bounds[i]) {
			prev[i][j].P = (i*dx < L0) ? PW0 : P0;
			prev[i][j].Pnet = (i*dx < L0) ? PW0 - S0 : 100000;
			prev[i][j].qL = 0; // (i*dx < L0) ? k / eta * (PW0 - P0) / WW0 : 0;
			prev[i][j].yf = 0; // (i*dx < L0) ? WW0 : 0;
		}

	// ������ ������� ��� ��
	for (size_t i = 0; i < la_prev_internal.size(); ++i)
		for each (size_t j in la_prev_internal[i]) {
			prev[i][j].P = (i*dx < L0) ? PW0 : P0;
			prev[i][j].Pnet = (i*dx < L0) ? PW0 - S0 : 100000;
			prev[i][j].qL = 0; // (i*dx < L0) ? k / eta * (PW0 - P0) / WW0 : 0;
			prev[i][j].yf = 0; // (i*dx < L0) ? WW0 : 0;
		}

	/*
	*  ���������� �����
	*/
	// ��������������� ��� ��� ���������
	double Sx = dy;
	double Sy = dx;
	double V = Sx * Sy;

	for (double t = 0; t <= T; t += dt) {
		system("cls");
		cout << (int)(100*t / T) << " %" << endl;
		// ������ ������� ���� ����� ���:
		// U = (Pnet[0,0], ... , Pnet[N,M], P[0,0], ... , P[N,M], P1[0,0], ... , P1[K,Q])^T
		// ��� NxM - ����������� ����� ������� (������ �������) �� X � Y,
		// KxQ - ����������� ��������� (������� �������).
		// ������ �� ������������ ���������� � ������� ������� U, ������� A �����������
		// �� ������������ ��������.

		// ��� ����������� ������� ���������� Pnet ���� (i, j) � ������� U ����������
		// �������� �� ���������� ����� � ������ �������.

		// ��� ����������� ������� ���������� P ���� (i, j) � ������� U ����������
		// �������� �� ���������� ����� � ������ ������� � ��������� � ���� �����������
		// ������ �������.

		// ��� ����������� ������� ���������� P1 ���� (i, j) � ������� U ����������
		// �������� �� ���������� ����� � ������� ������� � ��������� � ���� ���������
		// ����������� ������ �������.

		// �������� ��� ������� � ���������
		auto ua = current.get_upper_area(y_x);
		auto ua_bounds = ua.get_boundary();
		auto ua_curve_bound = ua.get_curve_boundary();
		auto ua_internal = ua.get_internal();
		auto ua_left_bound = ua.get_left_boundary();
		auto ua_right_bound = ua.get_right_boundary();
		auto ua_upper_bound = ua.get_upper_boundary();
		auto ua_lower_bound = ua.get_lower_boundary();

		auto la = current.get_low_area(y_x);
		auto la_bounds = la.get_boundary();
		auto la_left_bounds = la.get_left_boundary();
		auto la_curve_bound = la.get_curve_boundary();
		auto la_upper_bound = la.get_upper_boundary();
		auto la_lower_bound = la.get_lower_boundary();
		auto la_internal = la.get_internal();
		auto la_right_bounds = la.get_right_boundary();

		size_t la_size = la.get_nodes_count();

		// ����������� ����� ���������
		size_t eq_count = 0;

		// ����� ��������� (����������� ���������� �������)
		// �� 2 ��������� (��������� �� Pnet � �� P) �� ������ ���� ������
		// � �� ������ ��������� �� ������ ���� ������� �������
		size_t N = 2 * la.get_nodes_count() + ua.get_nodes_count();

		// ������� ������� ������� � ������ ������ �����
		Eigen::MatrixXf A = Eigen::MatrixXf::Zero(N, N);
		Eigen::VectorXf b(N);

		/*
		*  ��������� �� ��� ������� ������� (������)
		*/
		// �� �� Pnet �� ����� �������
		for each (size_t j in la_left_bounds) {
			size_t eq_num = la.get_number(0, j);
			A(eq_num, eq_num) = 1;
			b(eq_num) = PW0 - S0;
			eq_count++;
		}
		// �� �� P �� ����� �������
		for each (size_t j in la_left_bounds) {
			size_t eq_num = la_size + la.get_number(0, j);
			A(eq_num, eq_num) = 1;
			b(eq_num) = PW0;
			eq_count++;
		}

		/*
		*  ��������� �� ��� ��������� ������� (�������)
		*/
		// �� �� P1 �� ����� ������� (dP1/dx = 0)
		for each (size_t j in ua_left_bound) {
			size_t eq_num = 2 * la_size + ua.get_number(0, j);
			size_t P1_right = 2 * la_size + ua.get_number(1, j);
			A(eq_num, eq_num) = -1;
			A(eq_num, P1_right) = 1;
			b(eq_num) = 0;
			eq_count++;
		}
		// �� �� P1 �� ������� ������� � �������� (P1 = P �� ������� �������)
		for (size_t i = 1; i < ua_curve_bound.size() - 1; ++i)
			for each (size_t j in ua_curve_bound[i]) {
				size_t eq_num = 2 * la_size + ua.get_number(i, j);
				size_t P = la_size + la.get_number(i, j);
				A(eq_num, eq_num) = 1;
				A(eq_num, P) = -1;
				b(eq_num) = 0;
				eq_count++;
			}
		// �� �� P1 �� ������� � �������� ������ (P1 = P �� ������� �������)
		// ����� ������ ������ ������� ������ �������, ��� ��� ������ �������
		// ������� - ��� ������� ���� ��������� �������
		i = ua_curve_bound.size() - 1;
		for each (size_t j in la_right_bounds) {
			size_t eq_num = 2 * la_size + ua.get_number(i, j);
			size_t P = la_size + la.get_number(i, j);
			A(eq_num, eq_num) = 1;
			A(eq_num, P) = -1;
			b(eq_num) = 0;
			eq_count++;
		}
		// �� �� P1 �� ������ ������� (P1 = P0 - ���������)
		i = ua_bounds.size() - 1;
		for each (size_t j in ua_right_bound) {
			size_t eq_num = 2 * la_size + ua.get_number(i, j);
			A(eq_num, eq_num) = 1;
			b(eq_num) = P0;
			eq_count++;
		}
		// �� �� P1 �� ������� ������� (P1 = P0 - ���������)
		for (size_t i = 1; i < ua_upper_bound.size(); ++i)
			for each (size_t j in ua_upper_bound[i]) {
				size_t eq_num = 2 * la_size + ua.get_number(i, j);
				A(eq_num, eq_num) = 1;
				b(eq_num) = P0;
				eq_count++;
			}
		// �� �� ������ ����� �� ����� ������� �� ������� ���� ��������� �������
		// (dP1/dy = 0 �.�. ������ ������� ��������� ������� ��������� ������� �������)
		for (size_t i = ua_curve_bound.size(); i < ua_lower_bound.size() - 1; ++i)
			for each (size_t j in ua_lower_bound[i]) {
				size_t eq_num = 2 * la_size + ua.get_number(i, j);
				size_t P1_upper = 2 * la_size + ua.get_number(i, j + 1);
				A(eq_num, eq_num) = -1;
				A(eq_num, P1_upper) = 1;
				b(eq_num) = 0;
				eq_count++;
			}

		/*
		*  ������ ����� ���������
		*  ��� ������ ������� ���������� ��������� �� ���� ����� �����,
		*  ������� �������, ����� ����� ������� (��� ��). ������, ��
		*  ������ ������� ������� ���������� ������ ����� �����������, �.�.
		*  ������ ��� ��� �������.
		*  ��� ������� ������� ���������� ��������� ������ ������ �������
		*  (internal) (�� ���� �������� ������ ��).
		*/

		float fluxX = Sx / V / dx;

		// ��������� �� ������� ������� �������
		for (size_t i = 1; i < la_upper_bound.size() - 1; ++i)
			for each (size_t j in la_upper_bound[i]) {
				// ��������� �� Pnet
				size_t eq_Pnet_num = la.get_number(i, j);
				size_t P_right = la_size + la.get_number(i + 1, j);
				size_t P = la_size + la.get_number(i, j);
				size_t P_left = la_size + la.get_number(i - 1, j);
				size_t P1 = 2 * la_size + ua.get_number(i, j);
				size_t P1_top = 2 * la_size + ua.get_number(i, j + 1);

				A(eq_Pnet_num, eq_Pnet_num) = 1;
				A(eq_Pnet_num, P_right) = -w * dt * fluxX * pow(prev[i + 1][j].Pnet, 3);
				A(eq_Pnet_num, P) = w * dt * fluxX * (pow(prev[i][j].Pnet, 3)
					+ pow(prev[i][j].Pnet, 3));
				A(eq_Pnet_num, P_left) = -w * dt * fluxX * pow(prev[i - 1][j].Pnet, 3);
				A(eq_Pnet_num, P1) = -r * dt / dy;
				A(eq_Pnet_num, P1_top) = r * dt / dy;
				b(eq_Pnet_num) = prev[i][j].Pnet;

				// ��������� �� P (P - Pnet = S0)
				size_t eq_P_num = la_size + la.get_number(i, j);
				size_t Pnet = la.get_number(i, j);
				A(eq_P_num, eq_P_num) = 1;
				A(eq_P_num, Pnet) = -1;
				b(eq_P_num) = S0;

				eq_count += 2;
			}

		// ��������� �� ������ ������� ������� (�� �� �����, ��� �� �������,
		// ������ ��� qL, �.�. ��� ������� P1)
		for (size_t i = 1; i < la_lower_bound.size() - 1; ++i)
			for each (size_t j in la_lower_bound[i]) {
				// ��������� �� Pnet
				size_t eq_Pnet_num = la.get_number(i, j);
				size_t P_right = la_size + la.get_number(i + 1, j);
				size_t P = la_size + la.get_number(i, j);
				size_t P_left = la_size + la.get_number(i - 1, j);

				A(eq_Pnet_num, eq_Pnet_num) = 1;
				A(eq_Pnet_num, P_right) = -w * dt * fluxX * pow(prev[i + 1][j].Pnet, 3);
				A(eq_Pnet_num, P) = w * dt * fluxX * (pow(prev[i][j].Pnet, 3)
					+ pow(prev[i][j].Pnet, 3));
				A(eq_Pnet_num, P_left) = -w * dt * fluxX * pow(prev[i - 1][j].Pnet, 3);
				b(eq_Pnet_num) = prev[i][j].Pnet;

				// ��������� �� P (P - Pnet = S0)
				size_t eq_P_num = la_size + la.get_number(i, j);
				size_t Pnet = la.get_number(i, j);
				A(eq_P_num, eq_P_num) = 1;
				A(eq_P_num, Pnet) = -1;
				b(eq_P_num) = S0;

				eq_count += 2;
			}

		// ��������� �� ���������� ����� ������� (internal)
		// �� �� �����, ��� � �� ������ �������
		for (size_t i = 1; i < la_internal.size(); ++i)
			for each (size_t j in la_internal[i]) {
				// ��������� �� Pnet
				size_t eq_Pnet_num = la.get_number(i, j);
				size_t P_right = la_size + la.get_number(i + 1, j);
				size_t P = la_size + la.get_number(i, j);
				size_t P_left = la_size + la.get_number(i - 1, j);

				A(eq_Pnet_num, eq_Pnet_num) = 1;
				A(eq_Pnet_num, P_right) = -w * dt * fluxX * pow(prev[i + 1][j].Pnet, 3);
				A(eq_Pnet_num, P) = w * dt * fluxX * (pow(prev[i][j].Pnet, 3)
					+ pow(prev[i][j].Pnet, 3));
				A(eq_Pnet_num, P_left) = -w * dt * fluxX * pow(prev[i - 1][j].Pnet, 3);
				b(eq_Pnet_num) = prev[i][j].Pnet;

				// ��������� �� P (P - Pnet = S0)
				size_t eq_P_num = la_size + la.get_number(i, j);
				size_t Pnet = la.get_number(i, j);
				A(eq_P_num, eq_P_num) = 1;
				A(eq_P_num, Pnet) = -1;
				b(eq_P_num) = S0;

				eq_count += 2;
			}

		// ��������� �� ������ ������� ������� (������ ����� �����������)
		i = la_curve_bound.size() - 1;
		for each (size_t j in la_right_bounds) {
			// ��������� �� Pnet
			size_t eq_Pnet_num = la.get_number(i, j);
			size_t P = la_size + la.get_number(i, j);
			size_t P_left = la_size + la.get_number(i - 1, j);
			size_t P1 = 2 * la_size + ua.get_number(i, j);
			size_t P1_right = 2 * la_size + ua.get_number(i + 1, j);

			A(eq_Pnet_num, eq_Pnet_num) = 1;
			A(eq_Pnet_num, P) = w * dt * fluxX * pow(prev[i][j].Pnet, 3);
			A(eq_Pnet_num, P_left) = -w * dt * fluxX * pow(prev[i - 1][j].Pnet, 3);
			A(eq_Pnet_num, P1) = -r * dt / dx;
			A(eq_Pnet_num, P1_right) = r * dt / dx;
			b(eq_Pnet_num) = prev[i][j].Pnet;

			// ��������� �� P (P - Pnet = S0)
			size_t eq_P_num = la_size + la.get_number(i, j);
			size_t Pnet = la.get_number(i, j);
			A(eq_P_num, eq_P_num) = 1;
			A(eq_P_num, Pnet) = -1;
			b(eq_P_num) = S0;

			eq_count += 2;
		}

		// ��������� ������ ������� �������
		for (int i = 0; i < ua_internal.size(); ++i)
			for each(size_t j in ua_internal[i]) {
				size_t eq_num = 2 * la_size + ua.get_number(i, j);
				size_t P1_right = 2 * la_size + ua.get_number(i + 1, j);
				size_t P1_left = 2 * la_size + ua.get_number(i - 1, j);
				size_t P1_top = 2 * la_size + ua.get_number(i, j + 1);
				size_t P1_bot = 2 * la_size + ua.get_number(i, j - 1);

				float fluxX = X0 * dt * Sx / V / dx;
				float fluxY = X0 * dt * Sy / V / dy;

				A(eq_num, eq_num) = 1 + 2 * fluxX + 2 * fluxY;
				A(eq_num, P1_right) = -fluxX;
				A(eq_num, P1_left) = -fluxX;
				A(eq_num, P1_top) = -fluxY;
				A(eq_num, P1_bot) = -fluxY;

				b(eq_num) = prev[i][j].P1;

				eq_count++;
			}

		if (isPartialPivLu)
			b = A.partialPivLu().solve(b);
		else if (isColPivHouseholderQr)
			b = A.colPivHouseholderQr().solve(b);

		for each (size_t j in la_left_bounds) {
			size_t Pnet_eq_num = la.get_number(0, j);
			size_t P_eq_num = la_size + la.get_number(0, j);
			current[0][j].Pnet = b(Pnet_eq_num);
			current[0][j].P = b(P_eq_num);
		}

		for (size_t i = 1; i < la_upper_bound.size(); ++i)
			for each (size_t j in la_upper_bound[i]) {
				size_t Pnet_eq_num = la.get_number(i, j);
				size_t P_eq_num = la_size + la.get_number(i, j);
				current[i][j].Pnet = b(Pnet_eq_num);
				current[i][j].P = b(P_eq_num);
			}

		for (size_t i = 1; i < la_lower_bound.size(); ++i)
			for each (size_t j in la_lower_bound[i]) {
				size_t Pnet_eq_num = la.get_number(i, j);
				size_t P_eq_num = la_size + la.get_number(i, j);
				current[i][j].Pnet = b(Pnet_eq_num);
				current[i][j].P = b(P_eq_num);
			}

		for (size_t i = 1; i < la_internal.size(); ++i)
			for each (size_t j in la_internal[i]) {
				size_t Pnet_eq_num = la.get_number(i, j);
				size_t P_eq_num = la_size + la.get_number(i, j);
				current[i][j].Pnet = b(Pnet_eq_num);
				current[i][j].P = b(P_eq_num);
			}

		for (size_t i = 0; i < ua_bounds.size(); ++i)
			for each (size_t j in ua_bounds[i]) {
				size_t P1_eq_num = 2 * la_size + ua.get_number(i, j);
				current[i][j].P1 = b(P1_eq_num);
			}

		for (size_t i = 0; i < ua_internal.size(); ++i)
			for each (size_t j in ua_internal[i]) {
				size_t P1_eq_num = 2 * la_size + ua.get_number(i, j);
				current[i][j].P1 = b(P1_eq_num);
			}

		swap(prev, current);
	}

	swap(prev, current);

	//ofstream m_file("B.txt");
	//m_file << setprecision(6);
	//for (size_t i = 0; i < N; ++i) {
	//		m_file << b(i) << '\t';
	//	m_file << endl;
	//}
	//m_file.close();

	ofstream fileP1("P1.txt");
	for (int j = current.fv_count_y() - 1; j >= 0; j--) {
		for (size_t i = 0; i < current.fv_count_x(); ++i) {
			fileP1 << current[i][j].P1 << '\t';
		}
		fileP1 << endl;
	}
	fileP1.close();

	ofstream filePnet("Pnet.txt");
	for (int j = current.fv_count_y() - 1; j >= 0; j--) {
		for (size_t i = 0; i < current.fv_count_x(); ++i) {
			filePnet << current[i][j].Pnet << '\t';
		}
		filePnet << endl;
	}
	filePnet.close();

	////for (int d = 0; d < 50000; ++d) {

	//	// ��
	//	for each (size_t j in la_left_bounds) {
	//		current[0][j].Pnet = PW0 - S0;
	//		current[0][j].P = PW0;
	//		current[0][j].qL = k / eta * (PW0 - P0) / WW0;
	//		current[0][j].yf = WW0;
	//	}

	//	for (size_t i = 1; i < la_upper_bound.size() - 1; ++i)
	//		for (size_t q = 0; q < la_upper_bound[i].size(); ++q) {
	//			size_t j = la_upper_bound[i][q];
	//			current[i][j].Pnet = prev[i][j].Pnet
	//				+ h * h / eta / (E*E*E) * fluxX
	//				* (pow(prev[i + 1][j].Pnet, 3) * (prev[i + 1][j].P - prev[i][j].P) 
	//					- pow(prev[i - 1][j].Pnet, 3) * (prev[i][j].P - prev[i - 1][j].P))
	//				- 2 * m * E * dt / 3.14 / h * prev[i][j].qL;
	//			current[i][j].P = (current[i][j].Pnet > 200000) ? current[i][j].Pnet + S0 : P0;
	//			current[i][j].qL = 0; // k / eta * (current[i][j].P - P0) / WW0;
	//			//cout << current[i][j].qL << endl;
	//		}

	//	for (size_t i = 1; i < la_internal.size() - 1; ++i)
	//		for (size_t q = 0; q < la_internal[i].size(); ++q) {
	//			size_t j = la_internal[i][q];
	//			current[i][j].Pnet = prev[i][j].Pnet
	//				+ h * h / eta / (E*E*E) * fluxX
	//				* (pow(prev[i + 1][j].Pnet, 3) * (prev[i + 1][j].P - prev[i][j].P)
	//					- pow(prev[i - 1][j].Pnet, 3) * (prev[i][j].P - prev[i - 1][j].P))
	//				- 2 * m * E * dt / 3.14 / h * prev[i][j].qL;
	//			current[i][j].P = current[i][j].Pnet + S0;
	//			current[i][j].qL = 0; // k / eta * (current[i][j].P - P0) / WW0;
	//		}

		//swap(current, prev);
	//}

//	// ��������� ����� ��� ���������������� � ������� �����
//	double Sx = dy;
//	double Sy = dx;
//	double V = Sx * Sy;
//
//	for (double t = 0; t <= T; t += dt) {
//		cout << t << endl;
//
//		auto y_x = [Lx, t](float x) {return (x < 20) ? 3 : 33 - 3 * x / 2; };
//
//		auto ua = current.get_upper_area(y_x);
//		auto lower_boundary = ua.get_lower_boundary();
//		auto boundary = ua.get_boundary();
//		auto curve_bound = ua.get_curve_boundary();
//		auto intern = ua.get_internal();
//		auto top_bound = ua.get_upper_boundary();
//		auto right_bound = ua.get_right_boundary();
//
//		size_t N = ua.get_nodes_count();
//
//		Eigen::MatrixXf A = Eigen::MatrixXf::Zero(N, N);
//		Eigen::VectorXf b(N);
//
//		for (size_t i = 0; i < boundary.size(); ++i)
//			for each (size_t j in boundary[i]) {
//				size_t pos = ua.get_number(i, j);
//				A(pos, pos) = 1;
//				b(pos) = 200;
//			}
//
//		size_t last = current.fv_count_x();
//		for each (size_t j in right_bound) {
//			size_t pos = ua.get_number(last - 1, j);
//			size_t left_edge = ua.get_number(last - 2, j);
//			A(pos, pos) = -1;
//			A(pos, left_edge) = 1;
//			b(pos) = 0;
//		}
//
//		for (size_t i = 0; i < top_bound.size() - 1; ++i)
//			for each (size_t j in top_bound[i]) {
//				size_t pos = ua.get_number(i, j);
//				size_t bot_edge = ua.get_number(i, j - 1);
//				A(pos, pos) = 1;
//				A(pos, bot_edge) = -1;
//				b(pos) = 0;
//			}
//
//		for (size_t i = 0; i < curve_bound.size() - 1; ++i)
//			for each (size_t j in curve_bound[i]) {
//				size_t pos = ua.get_number(i, j);
//				A(pos, pos) = 1;
//				b(pos) = 300;
//			}
//
//#pragma omp parallel for
//		for (int i = 0; i < intern.size(); ++i)
//			for each(size_t j in intern[i]) {
//				size_t centr = ua.get_number(i, j);
//				size_t right_edge = ua.get_number(i + 1, j);
//				size_t left_edge = ua.get_number(i - 1, j);
//				size_t top_edge = ua.get_number(i, j + 1);
//				size_t bot_edge = ua.get_number(i, j - 1);
//
//				float fluxX = dt * Sx / V / dx;
//				float fluxY = dt * Sy / V / dy;
//
//				A(centr, centr) = 1 + 2 * fluxX + 2 * fluxY;
//				A(centr, right_edge) = -fluxX;
//				A(centr, left_edge) = -fluxX;
//				A(centr, top_edge) = -fluxY;
//				A(centr, bot_edge) = -fluxY;
//
//				b(centr) = prev[i][j].P1;
//			}
//
//		if (isPartialPivLu)
//			b = A.partialPivLu().solve(b);
//		else if (isColPivHouseholderQr)
//			b = A.colPivHouseholderQr().solve(b);
//
//		fill_from_vector(ua, b);
//		current.fill_from_area(ua);
//
//		swap(current, prev);
//	}
//


	//ofstream m_file("B.txt");
	//m_file << setprecision(6);
	//for (size_t i = 0; i < N; ++i) {
	//		m_file << b(i) << '\t';
	//	m_file << endl;
	//}
	//m_file.close();



	system("pause");
	return 0;
}