#pragma once

#include <vector>
#include <set>
#include <iostream>

#include "LAPACK_matrix.h"

// ��������� �� ������� v1 ��� �������� v2, ������� �� ��������������� ������
template <class T>
std::vector<std::vector<T>> vector2_diff(std::vector<std::vector<T>> &v1,
	std::vector<std::vector<T>> &v2) {

	std::vector<std::vector<T>> res;
	for (size_t i = 0; i < v1.size(); ++i) {
		std::vector<T> diff;
		set_difference(v1[i].begin(), v1[i].end(), v2[i].begin(), v2[i].end(),
			std::back_inserter(diff, diff.begin()));
		if (diff.size() > 0)
			res.push_back(diff);
	}

	return res;
}

// �������, ������������ � ������� ��� ������ ������� ������, ��������
// �������� �������� boundary, � ��������� ������ ������������ ������� �����.
// TODO: �������� size_t �� ushort ��� �������� ������ ��� �������� ���������
// ������� size_t �� ���������� ���������/������� �� std::pair<size_t, size_t>
template <class T>
class area {
	std::vector<std::vector<T>> _area;

	std::vector<std::vector<size_t>> _lower_boundary;
	std::vector<std::vector<size_t>> _upper_boundary;
	std::vector<size_t> _left_boundary;
	std::vector<size_t> _right_boundary;

	std::vector<std::vector<size_t>> _curve_boundary;

	std::vector<std::vector<size_t>> _full_boundary;
	std::vector<std::vector<size_t>> _internal;

	bool _is_low_curve = false;
	bool _is_upper_curve = false;

	bool _is_full_boundary_calc = false;
	bool _is_internal_calc = false;

	bool _is_lower_boundary_calc = false;
	bool _is_upper_boundary_calc = false;
	bool _is_left_boundary_calc = false;
	bool _is_right_boundary_calc = false;

public:
	area(std::vector<std::vector<T>> &area, bool is_is_lower_boundary,
		std::vector<std::vector<size_t>> &boundary) {

		_curve_boundary = boundary;

		_area = area;
		if (is_is_lower_boundary) 
			_is_low_curve = true;
		else 
			_is_upper_curve = true;
	}

	// �������� ������ ����� ����� ������� ������� ��������� �����
	size_t get_nodes_count() {
		size_t res = 0;

		get_boundary();
		get_internal();

		for (size_t i = 0; i < _full_boundary.size(); ++i)
			res += _full_boundary[i].size();
		for (size_t i = 0; i < _internal.size(); ++i)
			res += _internal[i].size();

		return res;
	}

	// �������� ���������� ����� ��������� �������� �������
	size_t get_number(size_t p, size_t q) {
		size_t res = 0;

		get_boundary();
		get_internal();

		if (p < 0 || p > _full_boundary.size())
			throw invalid_argument("p is outside from the area");
		if (q >= *_full_boundary[p].begin() && q <= *--_full_boundary[p].end()) {
			for (size_t i = 0; i < p; ++i)
				res += _internal[i].size() + _full_boundary[i].size();
			res += q - *_full_boundary[p].begin();
		}
		else
			throw invalid_argument("q is outside from the area");

		return res;
	}

	// �������� ������ �������� ��������� ����� �������
	std::vector<std::vector<size_t>> get_boundary() {
		if (!_is_full_boundary_calc) {

			get_upper_boundary();
			get_lower_boundary();
			get_left_boundary();
			get_right_boundary();

			if (_is_low_curve)
				_full_boundary.resize(_area.size());
			else
				_full_boundary.resize(_curve_boundary.size());

			*_full_boundary.begin() = _left_boundary;
			*--_full_boundary.end() = _right_boundary;

			for (size_t i = 1; i < _full_boundary.size() - 1; ++i) {
				_full_boundary[i].resize(_upper_boundary[i].size() 
					+ _lower_boundary[i].size());

				std::memcpy(_full_boundary[i].data(), _lower_boundary[i].data(), 
					_lower_boundary[i].size() * sizeof(size_t));

				std::memcpy(_full_boundary[i].data() + _lower_boundary[i].size(), 
					_upper_boundary[i].data(), _upper_boundary[i].size() * sizeof(size_t));

				std::sort(_full_boundary[i].begin(), _full_boundary[i].end());
			}

			_is_full_boundary_calc = true;
		}
			
		return _full_boundary;
	}

	// �������� ������ �������� ������� ������� �������
	std::vector<std::vector<size_t>> get_upper_boundary() {

		if (!_is_upper_boundary_calc) {
			size_t horizontal;

			if (_is_low_curve) {
				_upper_boundary.resize(_area.size());

				horizontal = _area[0].size() - 1;
				for (size_t i = 0; i < _area.size(); ++i)
					_upper_boundary[i].push_back(horizontal);
			}
			else {
				_upper_boundary.resize(_curve_boundary.size());
				for (size_t i = 0; i < _upper_boundary.size(); ++i)
					if (_curve_boundary[i].size() > 0) {
						_upper_boundary[i].resize(_curve_boundary[i].size());
						std::memcpy(_upper_boundary[i].data(), _curve_boundary[i].data(),
						_curve_boundary[i].size() * sizeof(size_t));
					}
			}

			_is_upper_boundary_calc = true;
		}
		return _upper_boundary;
	}

	// �������� ������ �������� ������ ������� �������
	std::vector<std::vector<size_t>> get_lower_boundary() {

		if (!_is_lower_boundary_calc) {
			size_t horizontal;

			if (_is_upper_curve) {
				_lower_boundary.resize(_curve_boundary.size());

				horizontal = 0;
				for (size_t i = 0; i < _lower_boundary.size(); ++i)
					_lower_boundary[i].push_back(horizontal);
			}
			else {
				_lower_boundary.resize(_area.size());

				for (size_t i = 0; i < _curve_boundary.size(); ++i)
					if (_curve_boundary[i].size() > 0) {
						_lower_boundary[i].resize(_curve_boundary[i].size());
						std::memcpy(_lower_boundary[i].data(), _curve_boundary[i].data(),
							_curve_boundary[i].size() * sizeof(size_t));
					}
					else
						_lower_boundary[i].push_back(0);
				for (size_t i = _curve_boundary.size(); i < _lower_boundary.size(); ++i)
					_lower_boundary[i].push_back(0);
			}

			_is_lower_boundary_calc = true;
		}

		return _lower_boundary;
	}

	// �������� ������ �������� ����� ������� �������
	std::vector<size_t> get_left_boundary() {

		if (!_is_left_boundary_calc) {

			size_t left_top;
			size_t left_bot;

			if (_is_low_curve) {
				get_lower_boundary();

				left_top = _area[0].size() - 1;
				left_bot = *(*_lower_boundary.begin()).begin();

			}
			else {
				get_upper_boundary();

				left_top = *(*_upper_boundary.begin()).begin();
				left_bot = 0;
			}

			for (size_t j = left_bot; j <= left_top; ++j)
				_left_boundary.push_back(j);

			_is_left_boundary_calc = true;
		}

		return _left_boundary;
	}

	// �������� ������ �������� ������ ������� �������
	std::vector<size_t> get_right_boundary() {

		if (!_is_right_boundary_calc) {
			
			size_t right_top;
			size_t right_bot;

			if (_is_low_curve) {
				get_lower_boundary();

				right_top = _area[0].size() - 1;
				right_bot = *(*--_lower_boundary.end()).begin();
				get_lower_boundary();
			}
			else {
				get_upper_boundary();

				right_top = *--(*--_upper_boundary.end()).end();
				right_bot = 0;
			}

			for (size_t j = right_bot; j <= right_top; ++j)
				_right_boundary.push_back(j);

			_is_right_boundary_calc = true;
		}

		return _right_boundary;
	}

	std::vector<std::vector<size_t>> get_curve_boundary() {
		return _curve_boundary;
	}

	std::vector<std::vector<size_t>> get_internal() {

		if (!_is_internal_calc) {
			get_boundary();

			_internal.resize(_full_boundary.size());
			get_boundary();
			for (size_t i = 0; i < _internal.size(); ++i) {

				size_t j0 = *_full_boundary[i].begin();
				int jn = *--_full_boundary[i].end();

				for (size_t j = 1; j < _full_boundary[i].size(); ++j)
					if (_full_boundary[i][j] == j0 + 1)
						j0++;
					else
						break;

				for (int j = _full_boundary[i].size() - 2; j > 0; --j)
					if (_full_boundary[i][j] == jn - 1)
						jn--;
					else
						break;

				for (int k = j0 + 1; k <= jn - 1; ++k)
					_internal[i].push_back(k);
			}

			_is_internal_calc = true;
		}
		
		return _internal;
	}

	std::vector<T> & operator[](size_t i) {
		return _area[i];
	}

	T & centr(size_t i, size_t j) {
		return _area[i][j];
	}
	T & right_edge(size_t i, size_t j) {
		return _area[i + 1][j];
	}
	T & left_edge(size_t i, size_t j) {
		return _area[i - 1][j];
	}
	T & top_edge(size_t i, size_t j) {
		return _area[i][j + 1];
	}
	T & bot_edge(size_t i, size_t j) {
		return _area[i][j - 1];
	}
};

