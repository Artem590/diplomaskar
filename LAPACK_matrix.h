#pragma once

#include <vector>

class LAPACK_matrix {
	size_t _M, _N;
	std::vector<double> _matrix;

public:
	LAPACK_matrix(size_t M, size_t N) {
		_M = M;
		_N = N;
		_matrix.resize(M * N, 0);
	}

	LAPACK_matrix(const LAPACK_matrix &from) {
		_M = from._M;
		_N = from._N;
		_matrix.resize(_M * _N);
		std::memcpy(_matrix.data(), from._matrix.data(), _M * _N * sizeof(double));
	}

	double& get(size_t i, size_t j) {
		return _matrix[i*_N + j];
	}

	size_t M() {
		return _M;
	}

	size_t N() {
		return _N;
	}

	double* data() {
		return _matrix.data();
	}

	double& operator()(size_t i, size_t j) {
		return _matrix[i*_N + j];
	}
};
